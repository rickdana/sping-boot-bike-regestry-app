import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

const httpOptions = {
  headers: new HttpHeaders({'content-type': 'application/json'})
};

@Injectable()
export class BikeService {

  constructor(private httpClient: HttpClient) { }

  getBikes() {
    return this.httpClient.get('/server/api/v1/bikes');
  }

}
